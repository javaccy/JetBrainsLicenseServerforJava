FROM java:8-jre-alpine
ENV PORT 8081
ADD target/client /client
CMD /client/bin/startup.sh -p $PORT