package com.vvvtimes.server;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.vvvtimes.util.rsasign;
 
public class MainServer extends AbstractHandler {
 
    public static void main ( String[] args ) throws Exception
    {
        Server server = new Server(8887);
        server.setHandler(new MainServer());
        server.start();
        server.join();
    }

    public void handle ( String target , Request baseRequest , HttpServletRequest request , HttpServletResponse response )
            throws IOException , ServletException
    {
        System.out.println(target);
        if ( target.equals("/") )
        {
            indexHandler(target, baseRequest, request, response);
        }
        else if ( target.equals("/rpc/ping.action") )
        {
            pingHandler(target, baseRequest, request, response);
        }
        else if ( target.equals("/rpc/obtainTicket.action") )
        {
            obtainTicketHandler(target, baseRequest, request, response);
        }
        else if ( target.equals("/rpc/releaseTicket.action") )
        {
            releaseTicketHandler(target, baseRequest, request, response);
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }

    }

    private void releaseTicketHandler(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        String salt = request.getParameter("salt");
        baseRequest.setHandled(true);
        if(salt==null){
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }else{
            String xmlContent = "<ReleaseTicketResponse><message></message><responseCode>OK</responseCode><salt>" + salt + "</salt></ReleaseTicketResponse>";
            String xmlSignature = rsasign.Sign(xmlContent);
            String body = "<!-- " + xmlSignature + " -->\n" + xmlContent;
            response.getWriter().print(body);
        }
    }

    private void obtainTicketHandler ( String target , Request baseRequest , HttpServletRequest request ,
            HttpServletResponse response ) throws IOException
    {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        SimpleDateFormat fm=new SimpleDateFormat("EEE,d MMM yyyy hh:mm:ss Z", Locale.ENGLISH);
        String date =fm.format(new Date())+" GMT";
        //response.setHeader("Date", date);
        //response.setHeader("Server", "fasthttp");
        String salt = request.getParameter("salt");
        String username = request.getParameter("userName");
        String prolongationPeriod = "607875500";
        baseRequest.setHandled(true);
        if(salt==null||username==null){
            response.setStatus(HttpServletResponse.SC_FORBIDDEN); 
        }else{
            String xmlContent = "<ObtainTicketResponse><message></message><prolongationPeriod>" + prolongationPeriod + "</prolongationPeriod><responseCode>OK</responseCode><salt>" + salt + "</salt><ticketId>1</ticketId><ticketProperties>licensee=" + username + "\tlicenseType=0\t</ticketProperties></ObtainTicketResponse>";
            String xmlSignature = rsasign.Sign(xmlContent);
            String body = "<!-- " + xmlSignature + " -->\n" + xmlContent;
            response.getWriter().print(body);
        }
        
        
    }

    private void pingHandler ( String target , Request baseRequest , HttpServletRequest request , HttpServletResponse response ) throws IOException
    {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        String salt = request.getParameter("salt");
        baseRequest.setHandled(true);
        if(salt==null){
            response.setStatus(HttpServletResponse.SC_FORBIDDEN); 
        }else{
            String xmlContent = "<PingResponse><message></message><responseCode>OK</responseCode><salt>" + salt + "</salt></PingResponse>";
            String xmlSignature = rsasign.Sign(xmlContent);
            String body = "<!-- " + xmlSignature + " -->\n" + xmlContent;
            response.getWriter().print(body);
        }
        
    }

    private void indexHandler ( String target , Request baseRequest , HttpServletRequest request , HttpServletResponse response ) throws IOException
    {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        response.getWriter().println("<h1>Hello,This is a JetBrains License Server!</h1>");
        
    }
}